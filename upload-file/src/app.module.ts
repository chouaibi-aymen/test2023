import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Test } from './test/test.entity';
import { TestModule } from './test/test.module';

@Module({
  imports: [
      TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: '',
          database: 'test',
          entities: [Test],
          synchronize: true,
      }),
      
      TestModule,
  ],
  
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
